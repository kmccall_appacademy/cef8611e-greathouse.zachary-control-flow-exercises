# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |char|
    if char == char.downcase
      str.delete!(char)
    end
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  middle = ""

  if str.length % 2 == 1
    middle += str[str.length / 2]
  else
    part_one = str[0...str.length / 2]
    part_two = str[str.length / 2..-1]

    middle += part_one[-1] + part_two[0]
  end

  middle
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char do |char|
    if VOWELS.include? char
      count += 1
    end
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  i = 1
  while i <= num
    product *= i
    i += 1
  end
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined_arr = ""

  arr.each do |el|
    if el != arr.last
      joined_arr += el + separator
    else
      joined_arr += el
    end
  end

  joined_arr
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  split_str = str.split("")

  split_str.each_with_index do |el, i|
    if i % 2 == 0
      el.downcase!
    else
      el.upcase!
    end
  end

  split_str.join("")
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  split_str = str.split(" ")

  split_str.each do |word|
    if word.length >= 5
      word.reverse!
    end
  end

  split_str.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = []

  for num in 1..n do
    if num % 3 == 0 && num % 5 != 0
      arr.push("fizz")
    elsif num % 3 != 0 && num % 5 == 0
      arr.push("buzz")
    elsif num % 3 == 0 && num % 5 == 0
      arr.push("fizzbuzz")
    else
      arr.push(num)
    end
  end
  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num < 2
    return false
  end

  i = 2
  while i < num
    if num % i == 0
      return false
    end
    i += 1
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors_arr = []

  i = 1
  while i <= num
    if num % i == 0
      factors_arr.push(i)
    end
    i += 1
  end

  factors_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factors_arr = []

  i = 1
  while i <= num
    if prime?(i) && num % i == 0
      prime_factors_arr.push(i)
    end
    i += 1
  end

  prime_factors_arr
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  count = 0

  i = 1
  while i <= num
    if prime?(i) && num % i == 0
      count += 1
    end
    i += 1
  end

  count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)

def even?(num)
  num % 2 == 0
end

def even_count(arr)
  count = 0

  arr.each do |el|
    if el % 2 == 0
      count += 1
    end
  end
  count
end

def odd_count(arr)
  count = 0

  arr.each do |el|
    if el % 2 == 1
      count += 1
    end
  end
  count
end

def first_even_number(arr)
  arr.each do |el|
    if el % 2 == 0
      return el
    end
  end
end

def first_odd_number(arr)
  arr.each do |el|
    if el % 2 == 1
      return el
    end
  end
end


def oddball(arr)
  if even_count(arr) == 1
    first_even_number(arr)
  elsif odd_count(arr) == 1
    first_odd_number(arr)
  end
end
